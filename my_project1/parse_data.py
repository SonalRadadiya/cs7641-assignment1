# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

#read data file into csv
df_bcw = pd.read_csv('./breast-cancer-wisconsin.data', header=None)
df_ww = pd.read_csv('./thyroid.dat', header=None)

#assign column names
df_bcw.columns = ['sample_code','clump_thck', 'cell_unif_size', 'cell_unif_shape', 'marg_adhes', 'se_cellsize', 'bare_nuclei', 'bland_chromatin', 'norm_nucleoli', 'mitoses', 'class']
df_ww.columns = ['Age', 'Sex', 'On_thyroxine', 'Query_on_thyroxine', 'On_antithyroid_medication', 'Sick', 'Pregnant', 'Thyroid_surgery', 'I131_treatment', 'Query_hypothyroid', 'Query_hyperthyroid',
                 'Lithium', 'Goitre', 'Tumor', 'Hypopituitary', 'Psych', 'TSH', 'T3', 'TT4', 'T4U', 'FTI', 'Class']

#substitute missing values and drop index column if any
df_bcw.replace('?', -99999, inplace=True)
df_bcw.drop(['sample_code'], 1, inplace=True)

#data type conversion where required
df_bcw['bare_nuclei'] = df_bcw['bare_nuclei'].astype(float)
df_bcw['clump_thck'] = df_bcw['clump_thck'].astype(float)
df_bcw['cell_unif_size'] = df_bcw['cell_unif_size'].astype(float)
df_bcw['cell_unif_shape'] = df_bcw['cell_unif_shape'].astype(float)
df_bcw['marg_adhes'] = df_bcw['marg_adhes'].astype(float)
df_bcw['se_cellsize'] = df_bcw['se_cellsize'].astype(float)
df_bcw['bare_nuclei'] = df_bcw['bare_nuclei'].astype(float)
df_bcw['bland_chromatin'] = df_bcw['bland_chromatin'].astype(float)
df_bcw['norm_nucleoli'] = df_bcw['norm_nucleoli'].astype(float)
df_bcw['mitoses'] = df_bcw['mitoses'].astype(float)
df_bcw['class'] = df_bcw['class'].astype(float)

df_ww['Age'] = df_ww['Age'].astype(float)
df_ww['Sex'] = df_ww['Sex'].astype(float)
df_ww['On_thyroxine'] = df_ww['On_thyroxine'].astype(float)
df_ww['Query_on_thyroxine'] = df_ww['Query_on_thyroxine'].astype(float)
df_ww['On_antithyroid_medication'] = df_ww['On_antithyroid_medication'].astype(float)
df_ww['Sick'] = df_ww['Sick'].astype(float)
df_ww['Pregnant'] = df_ww['Pregnant'].astype(float)
df_ww['Thyroid_surgery'] = df_ww['Thyroid_surgery'].astype(float)
df_ww['I131_treatment'] = df_ww['I131_treatment'].astype(float)
df_ww['Query_hypothyroid'] = df_ww['Query_hypothyroid'].astype(float)
df_ww['Query_hyperthyroid'] = df_ww['Query_hyperthyroid'].astype(float)
df_ww['Lithium'] = df_ww['Lithium'].astype(float)
df_ww['Goitre'] = df_ww['Goitre'].astype(float)
df_ww['Tumor'] = df_ww['Tumor'].astype(float)
df_ww['Hypopituitary'] = df_ww['Hypopituitary'].astype(float)
df_ww['Psych'] = df_ww['Psych'].astype(float)
df_ww['TSH'] = df_ww['TSH'].astype(float)
df_ww['T3'] = df_ww['T3'].astype(float)
df_ww['TT4'] = df_ww['TT4'].astype(float)
df_ww['T4U'] = df_ww['T4U'].astype(float)
df_ww['FTI'] = df_ww['FTI'].astype(float)
df_ww['Class'] = df_ww['Class'].astype(float)

plt.figure()
#df_bcw['class'].hist(by=df_bcw['class'])
#df_bcw.groupby().hist()
df_bcw['class'].hist(bins=10, color='b',rwidth=70)
plt.xticks(range(2,4))
plt.xlim([2, 4])
plt.xlabel("2-benign, 4-malignant")
plt.ylabel("Number of samples")
plt.title("Class distribution of breat cancer dataset")
plt.show()
plt.savefig("bcw_class_distr.pdf")

plt.figure()
#df_bcw['class'].hist(by=df_bcw['class'])
#df_bcw.groupby().hist()
df_ww['Class'].hist(bins=10, color='b',rwidth=70)
plt.xticks(range(1,3))
plt.xlim([1, 3])
plt.xlabel("1-normal, 2-hyperthyroidism, 3-hypothyroidism")
plt.ylabel("Number of samples")
plt.title("Class distribution of thyroid dataset")
plt.show()
plt.savefig("ww_class_distr.pdf")

#699 total samples, 2-benign (65.52%), 4-malignant (34.47%)
#unique, counts = np.unique(df_bcw['class'], return_counts=True)
#print (dict(zip(unique, counts)))

#7206 total samples, 1-normal (2.38%), 2-hyperthyroidism (5.10%), 3-hypothyroidism (92.5%)
#unique, counts = np.unique(df_ww['Class'], return_counts=True)
#print (dict(zip(unique, counts)))


#put data into hdf file
#df_bcw.to_hdf('datasets.hdf','bcw')
#df_ww.to_hdf('datasets.hdf', 'ww')
