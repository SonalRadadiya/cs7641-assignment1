# -*- coding: utf-8 -*-

import numpy as np
import sklearn.model_selection as ms
from sklearn.neighbors import KNeighborsClassifier as knnC
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight
from generate_curves import generateTimingCurve, generateLearningCurve, generateValidationCurve


def balanced_accuracy(truth,pred):
    wts = compute_sample_weight('balanced',truth)
    return accuracy_score(truth,pred,sample_weight=wts)

scorer = make_scorer(balanced_accuracy)

# Load Data
bcw = pd.read_hdf('datasets.hdf','bcw')
bcwX = bcw.drop('class',1).copy().values
bcwY = bcw['class'].copy().values

ww = pd.read_hdf('datasets.hdf','ww')
wwX = ww.drop('Class',1).copy().values
wwY = ww['Class'].copy().values

bcw_trainX, bcw_testX, bcw_trainY, bcw_testY = ms.train_test_split(bcwX, bcwY, test_size=0.2, random_state=4,stratify=bcwY)
ww_trainX, ww_testX, ww_trainY, ww_testY = ms.train_test_split(wwX, wwY, test_size=0.2, random_state=0,stratify=wwY)

d = bcwX.shape[1]
hiddens_bcw = [(h,)*l for l in [1,2,3] for h in [d,d//2,d*2]]
alphas = [10**-x for x in np.arange(1,9.01,1/2)]
d = wwX.shape[1]
hiddens_ww = [(h,)*l for l in [1,2,3] for h in [d,d//2,d*2]]

pipeB = Pipeline([('Scale',StandardScaler()),
                 ('KNN',knnC())])  

pipeW = Pipeline([('Scale',StandardScaler()),
                 ('KNN',knnC())])


params_bcw= {'KNN__metric':['manhattan','euclidean','chebyshev','minkowski'],'KNN__n_neighbors':np.arange(1,51,5),'KNN__weights':['uniform','distance']}
params_ww= {'KNN__metric':['manhattan','euclidean','chebyshev','minkowski'],'KNN__n_neighbors':np.arange(1,51,5),'KNN__weights':['uniform','distance']}

cvB = ms.GridSearchCV(pipeB,n_jobs=-1,param_grid=params_bcw,refit=True,verbose=10,cv=2,scoring=scorer)
cvW = ms.GridSearchCV(pipeW,n_jobs=-1,param_grid=params_ww,refit=True,verbose=10,cv=2,scoring=scorer)

generateLearningCurve(cvB, bcwX, bcwY, bcw_trainX, bcw_trainY, bcw_testX, bcw_testY, 'KNN', 'breast-cancer-wisconsin')
generateLearningCurve(cvW, wwX, wwY, ww_trainX, ww_trainY, ww_testX, ww_testY, 'KNN', 'thyroid')

#tuning hyper parameters and generate validation curves
#train_scores, valid_scores = ms.validation_curve(knnC(), bcwX, bcwY, param_name="n_neighbors", param_range=np.arange(1,51,3), cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'KNN', 'breast-cancer-wisconsin', 'n_neighbors', np.arange(1,51,3))
#train_scores, valid_scores = ms.validation_curve(knnC(), bcwX, bcwY, param_name="metric", param_range=['manhattan','euclidean','chebyshev'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'KNN', 'breast-cancer-wisconsin', 'metric', [1,2,3])

#tuning hyper parameters and generate validation curves
#train_scores, valid_scores = ms.validation_curve(knnC(), wwX, wwY, param_name="n_neighbors", param_range=np.arange(1,51,3), cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'KNN', 'thyroid', 'n_neighbors', np.arange(1,51,3))
#train_scores, valid_scores = ms.validation_curve(knnC(), wwX, wwY, param_name="metric", param_range=['manhattan','euclidean','chebyshev'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'KNN', 'thyroid', 'metric', [1,2,3])

bcw_final_params=cvB.best_params_
ww_final_params=cvW.best_params_

pipeB.set_params(**bcw_final_params)
pipeW.set_params(**ww_final_params)

generateTimingCurve(bcwX,bcwY,pipeB,'KNN','breast-cancer-wisconsin')
generateTimingCurve(bcwX,bcwY,pipeW,'KNN','thyroid')

