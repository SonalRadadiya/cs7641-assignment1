# -*- coding: utf-8 -*-
"""
author: Sonal Radadiya

"""
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from sklearn import learning_curve
import pandas as pd
import numpy as np
import sklearn.model_selection as ms
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight
from collections import defaultdict
from time import clock



def balanced_accuracy(truth,pred):
    wts = compute_sample_weight('balanced',truth)
    return accuracy_score(truth,pred,sample_weight=wts)

scorer = make_scorer(balanced_accuracy)


def generateLearningCurve(cvobj,X,y,trainX,trainY,testX,testY,clf_type=None,dataset=None):
    np.random.seed(55)
    if clf_type is None or dataset is None:
        raise

    cvobj.fit(trainX, trainY)
    regTable = pd.DataFrame(cvobj.cv_results_)
    regTable.to_csv('./output/{}_{}_reg.csv'.format(clf_type,dataset),index=False)
    test_score = cvobj.score(testX,testY)
    with open('./output/test_results_best_parameters.csv','a') as f:
        f.write('{},{},{},{}\n'.format(clf_type,dataset,test_score,cvobj.best_params_))

    N = trainY.shape[0]
    train_sizes, train_scores, test_scores = ms.learning_curve(cvobj.best_estimator_,X,y,cv=5,train_sizes=[int(N*x/8) for x in range(1,8)],verbose=10,scoring=scorer)

    df_train_scores = pd.DataFrame(index = train_sizes,data = train_scores)
    df_test_scores  = pd.DataFrame(index = train_sizes,data = test_scores)

    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    df_train_scores.to_csv('./output/{}_{}_train_learningScore.csv'.format(clf_type,dataset))
    df_test_scores.to_csv('./output/{}_{}_test_learningScore.csv'.format(clf_type,dataset))

    plt.figure()

    plt.plot(train_sizes, train_scores_mean, 'o-', color="r", label="Training score", linewidth=1)
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g", label="Cross-validation score", linewidth=1)
    plt.legend()
    plt.title("Learning curve for {} on {} dataset".format(clf_type,dataset))
    plt.legend(loc="best")
    plt.xlabel("Number of training examples")
    plt.ylabel("Prediction accuracy score")

    #plt.gca().invert_yaxis()
    # box-like grid
    plt.grid()

    # plot the std deviation as a transparent range at each training set size
    plt.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.1, color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.1, color="g")

    # sizes the window for readability and displays the plot
    # shows error from 0 to 1.1
    #plt.ylim(-.1,1.1)
    #plt.show()
    plt.savefig('./output/{}_{}learning_curve.pdf'.format(clf_type, dataset))
    return

def generateValidationCurve(train_scores, valid_scores, clf_type, dataset, param_name, param_values):

    df_train_scores = pd.DataFrame(index = param_values,data = train_scores)
    df_valid_scores  = pd.DataFrame(index = param_values,data = valid_scores)

    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    valid_scores_mean = np.mean(valid_scores, axis=1)
    valid_scores_std = np.std(valid_scores, axis=1)


    df_train_scores.to_csv('./output/{}_{}_{}_{}_train_validationScore.csv'.format(clf_type,param_name,dataset,param_name))
    df_valid_scores.to_csv('./output/{}_{}_{}_{}_test_validationScore.csv'.format(clf_type,param_name,dataset,param_name))

    plt.figure()
    #linewidth=1
    plt.plot(param_values, train_scores_mean, 'o-', color="c", label="Training score", linewidth=1)
    plt.plot(param_values, valid_scores_mean, 'o-', color="y", label="Cross-validation score", linewidth=1)
    plt.legend()
    plt.title("Validation curve for {} on {} dataset".format(clf_type, dataset))
    plt.legend(loc="best")
    plt.xlabel("Hyperparameter: {}".format(param_name))
    plt.ylabel("Prediction accuracy score")

    #plt.gca().invert_yaxis()
    # box-like grid
    plt.grid()

    # plot the std deviation as a transparent range at each training set size
    plt.savefig('./output/{}_{}_{}_validation_curve.pdf'.format(clf_type,param_name,dataset))
    return


def generateTimingCurve(X,Y,clf,clfName,dataset):
    out = defaultdict(dict)
    for frac in [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]:
        X_train, X_test, y_train, y_test = ms.train_test_split(X, Y, test_size=frac, random_state=42)
        st = clock()
        np.random.seed(55)
        clf.fit(X_train,y_train)
        out['train'][frac]= clock()-st
        st = clock()
        clf.predict(X_test)
        out['test'][frac]= clock()-st
        print(clfName,dataset,frac)
    out = pd.DataFrame(out)
    out.to_csv('./output/timingcurve.csv')


    plt.figure()
    data = np.genfromtxt('./output/timingcurve.csv', delimiter=',',skip_header=1 ,names = ['frac', 'train', 'test'])

    plt.plot(data['frac'], data['train'], 'o-', color="m", label="Training time")
    plt. plot(data['frac'], data['test'], 'o-', color="g", label="Testing time")
    plt.legend()
    plt.title("Timing curve for {} on {} dataset".format(clfName,dataset))
    plt.legend(loc="best")
    plt.xlabel("Testset fraction")
    plt.ylabel("Time in seconds")
    #plt.gca().invert_yaxis()
    # box-like grid
    plt.grid()
    plt.savefig('./output/{}_{}_timingcurve.pdf'.format(clfName, dataset))
    return

def iterationLC(clfObj,trgX,trgY,tstX,tstY,params,clf_type=None,dataset=None):
    np.random.seed(55)
    if clf_type is None or dataset is None:
        raise
    cv = ms.GridSearchCV(clfObj,n_jobs=1,param_grid=params,refit=True,verbose=10,cv=5,scoring=scorer)
    cv.fit(trgX,trgY)
    regTable = pd.DataFrame(cv.cv_results_)
    regTable.to_csv('./output/ITER_base_{}_{}.csv'.format(clf_type,dataset),index=False)
    d = defaultdict(list)
    name = list(params.keys())[0]
    for value in list(params.values())[0]:
        d['param_{}'.format(name)].append(value)
        clfObj.set_params(**{name:value})
        clfObj.fit(trgX,trgY)
        pred = clfObj.predict(trgX)
        d['train acc'].append(balanced_accuracy(trgY,pred))
        clfObj.fit(trgX,trgY)
        pred = clfObj.predict(tstX)
        d['test acc'].append(balanced_accuracy(tstY,pred))
        print(value)
    d = pd.DataFrame(d)
    d.to_csv('./output/ITERtestSET_{}_{}.csv'.format(clf_type,dataset),index=False)
    return cv
