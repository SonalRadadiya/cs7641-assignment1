# -*- coding: utf-8 -*-


import sklearn.model_selection as ms
from sklearn.ensemble import AdaBoostClassifier
from helpers import dtclf_pruned
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight
from generate_curves import generateTimingCurve, generateLearningCurve, iterationLC, generateValidationCurve


def balanced_accuracy(truth,pred):
    wts = compute_sample_weight('balanced',truth)
    return accuracy_score(truth,pred,sample_weight=wts)

scorer = make_scorer(balanced_accuracy)

# Load Data
bcw = pd.read_hdf('datasets.hdf','bcw')
bcwX = bcw.drop('class',1).copy().values
bcwY = bcw['class'].copy().values

ww = pd.read_hdf('datasets.hdf','ww')
wwX = ww.drop('Class',1).copy().values
wwY = ww['Class'].copy().values

bcw_trainX, bcw_testX, bcw_trainY, bcw_testY = ms.train_test_split(bcwX, bcwY, test_size=0.2, random_state=4,stratify=bcwY)
ww_trainX, ww_testX, ww_trainY, ww_testY = ms.train_test_split(wwX, wwY, test_size=0.2, random_state=0,stratify=wwY)

alphas = [-1,-1e-3,-(1e-3)*10**-0.5, -1e-2, -(1e-2)*10**-0.5,-1e-1,-(1e-1)*10**-0.5, 0, (1e-1)*10**-0.5,1e-1,(1e-2)*10**-0.5,1e-2,(1e-3)*10**-0.5,1e-3]

bcw_base = dtclf_pruned(criterion='gini',class_weight='balanced',random_state=55)
ww_base = dtclf_pruned(criterion='entropy',class_weight='balanced',random_state=55)

paramsB= {'Boost__n_estimators':[1,2,5,10,20,30,45,60,80,100],
          'Boost__base_estimator__alpha':alphas,
          'Boost__learning_rate':[(2**x)/100 for x in range(8)]+[1]}


paramsW = {'Boost__n_estimators':[1,2,5,10,20,30,45,60,80,100],
           'Boost__base_estimator__alpha':alphas,
           'Boost__learning_rate':[(2**x)/100 for x in range(8)]+[1]}
         
bcw_booster = AdaBoostClassifier(algorithm='SAMME',base_estimator=bcw_base,random_state=55)
ww_booster = AdaBoostClassifier(algorithm='SAMME',base_estimator=ww_base,random_state=55)

pipeB = Pipeline([('Scale',StandardScaler()),
                 ('Boost',bcw_booster)])

pipeW = Pipeline([('Scale',StandardScaler()),
                 ('Boost',ww_booster)])

cvB = ms.GridSearchCV(pipeB,n_jobs=-1,param_grid=paramsB,refit=True,verbose=10,cv=2,scoring=scorer)
cvW = ms.GridSearchCV(pipeW,n_jobs=-1,param_grid=paramsW,refit=True,verbose=10,cv=2,scoring=scorer)

generateLearningCurve(cvB, bcwX, bcwY, bcw_trainX, bcw_trainY, bcw_testX, bcw_testY, 'Boosting', 'breast-cancer-wisconsin')
generateLearningCurve(cvW, wwX, wwY, ww_trainX, ww_trainY, ww_testX, ww_testY, 'Boosting', 'thyroid')

#tuning hyper parameters and generate validation curves
#train_scores, valid_scores = ms.validation_curve(bcw_booster, bcwX, bcwY, param_name="n_estimators", param_range=[1,2,5,10,20,30,45,60,80,100], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'Boosting', 'breast-cancer-wisconsin', 'n_estimators', [1,2,5,10,20,30,45,60,80,100])
#train_scores, valid_scores = ms.validation_curve(bcw_booster, bcwX, bcwY, param_name="learning_rate", param_range=[1,2,3,4,5], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'Boosting', 'breast-cancer-wisconsin', 'learning_rate', [1,2,3,4,5])

#train_scores, valid_scores = ms.validation_curve(ww_booster, wwX, wwY, param_name="n_estimators", param_range=[1,2,5,10,20,30,45,60,80,100], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'Boosting', 'thyroid', 'n_estimators', [1,2,5,10,20,30,45,60,80,100])
#train_scores, valid_scores = ms.validation_curve(ww_booster, wwX, wwY, param_name="learning_rate", param_range=[1,2], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'Boosting', 'thyroid', 'learning_rate', [1,2])

bcw_final_params = cvB.best_params_
ww_final_params = cvW.best_params_

pipeB.set_params(**bcw_final_params)
pipeW.set_params(**ww_final_params)

generateTimingCurve(bcwX,bcwY,pipeB,'Boosting','breast-cancer-wisconsin')
generateTimingCurve(wwX,wwY,pipeW,'Boosting','thyroid')

pipeB.set_params(**bcw_final_params)
iterationLC(pipeB,bcw_trainX,bcw_trainY,bcw_testX,bcw_testY,{'Boost__n_estimators':[1,2,5,10,20,30,40,50,60,70,80,90,100]},'Boost','breast-cancer-wisconsin')
pipeW.set_params(**ww_final_params)
iterationLC(pipeW,ww_trainX,ww_trainY,ww_testX,ww_testY,{'Boost__n_estimators':[1,2,5,10,20,30,40,50]},'Boost','thyroid')
