# -*- coding: utf-8 -*-

import numpy as np
import sklearn.model_selection as ms
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import euclidean_distances
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.utils.multiclass import unique_labels
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.linear_model import SGDClassifier
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight
from generate_curves import generateTimingCurve, generateLearningCurve, iterationLC, generateValidationCurve


def balanced_accuracy(truth,pred):
    wts = compute_sample_weight('balanced',truth)
    return accuracy_score(truth,pred,sample_weight=wts)

scorer = make_scorer(balanced_accuracy)


class primalSVM_RBF(BaseEstimator, ClassifierMixin):
    '''http://scikit-learn.org/stable/developers/contributing.html'''
    
    def __init__(self, alpha=1e-9,gamma_frac=0.1,n_iter=2000):
         self.alpha = alpha
         self.gamma_frac = gamma_frac
         self.n_iter = n_iter
         
    def fit(self, X, y):
         # Check that X and y have correct shape
         X, y = check_X_y(X, y)
         
         # Get the kernel matrix
         dist = euclidean_distances(X,squared=True)
         median = np.median(dist) 
         del dist
         gamma = median
         gamma *= self.gamma_frac
         self.gamma = 1/gamma
         kernels = rbf_kernel(X,None,self.gamma )
         
         self.X_ = X
         self.classes_ = unique_labels(y)
         self.kernels_ = kernels
         self.y_ = y
         self.clf = SGDClassifier(loss='hinge',penalty='l2',alpha=self.alpha,
                                  l1_ratio=0,fit_intercept=True,verbose=False,
                                  average=False,learning_rate='optimal',
                                  class_weight='balanced',n_iter=self.n_iter,
                                  random_state=55)         
         self.clf.fit(self.kernels_,self.y_)
         
         # Return the classifier
         return self

    def predict(self, X):
         # Check is fit had been called
         check_is_fitted(self, ['X_', 'y_','clf','kernels_'])
         # Input validation
         X = check_array(X)
         new_kernels = rbf_kernel(X,self.X_,self.gamma )
         pred = self.clf.predict(new_kernels)
         return pred
    
# Load Data
bcw = pd.read_hdf('datasets.hdf','bcw')
bcwX = bcw.drop('class',1).copy().values
bcwY = bcw['class'].copy().values

ww = pd.read_hdf('datasets.hdf','ww')
wwX = ww.drop('Class',1).copy().values
wwY = ww['Class'].copy().values

bcw_trainX, bcw_testX, bcw_trainY, bcw_testY = ms.train_test_split(bcwX, bcwY, test_size=0.2, random_state=4,stratify=bcwY)
ww_trainX, ww_testX, ww_trainY, ww_testY = ms.train_test_split(wwX, wwY, test_size=0.2, random_state=0,stratify=wwY)

N_bcw = bcw_trainX.shape[0]
N_ww = ww_trainX.shape[0]

alphas = [10**-x for x in np.arange(1,9.01,1/2)]
"""

#Linear SVM
pipeB = Pipeline([('Scale',StandardScaler()),
                 ('SVM',SGDClassifier(loss='hinge', l1_ratio=0,penalty='l2',class_weight='balanced',random_state=55))])

pipeW = Pipeline([('Scale',StandardScaler()),
                 ('SVM',SGDClassifier(loss='hinge', l1_ratio=0,penalty='l2',class_weight='balanced',random_state=55))])

params_bcw = {'SVM__alpha':alphas,'SVM__n_iter':[int((1e6/N_bcw)/.8)+1]}
#params_ww = {'SVM__alpha':alphas,'SVM__n_iter':[int((1e6/N_ww)/.8)+1]}

cvB = ms.GridSearchCV(pipeB,n_jobs=1,param_grid=params_bcw,refit=True,verbose=10,cv=2,scoring=scorer)
#cvW = ms.GridSearchCV(pipeW,n_jobs=1,param_grid=params_ww,refit=True,verbose=10,cv=2,scoring=scorer)

generateLearningCurve(cvB, bcwX, bcwY, bcw_trainX, bcw_trainY, bcw_testX, bcw_testY, 'SVM', 'breast-cancer-wisconsin')
#generateLearningCurve(cvW, wwX, wwY, ww_trainX, ww_trainY, ww_testX, ww_testY, 'SVM', 'thyroid')

bcw_final_params = cvB.best_params_
ww_final_params = cvW.best_params_

pipeB.set_params(**bcw_final_params)
#pipeW.set_params(**ww_final_params)

generateTimingCurve(bcwX,bcwY,pipeB,'SVM','breast-cancer-wisconsin')
#generateTimingCurve(wwX,wwY,pipeW,'SVM','thyroid')

pipeB.set_params(**bcw_final_params)
iterationLC(pipeB,bcw_trainX,bcw_trainY,bcw_testX,bcw_testY,{'SVM__n_iter':[2**x for x in range(12)]},'SVM_Lin','breast-cancer-wisconsin')
#pipeW.set_params(**ww_final_params)
#iterationLC(pipeW,ww_trainX,ww_trainY,ww_testX,ww_testY,{'SVM__n_iter':np.arange(1,75,3)},'SVM_Lin','thyroid')


"""
#RBF SVM
gamma_fracsB = np.arange(0.2,2.1,0.2)
gamma_fracsW = np.arange(0.05,1.01,0.1)

pipeB = Pipeline([('Scale',StandardScaler()),
                 ('SVM',primalSVM_RBF())])

pipeW = Pipeline([('Scale',StandardScaler()),
                 ('SVM',primalSVM_RBF())])

params_bcw = {'SVM__alpha':alphas, 'SVM__gamma_frac':gamma_fracsB}
params_ww = {'SVM__alpha':alphas,'SVM__gamma_frac':gamma_fracsW}
#
cvB = ms.GridSearchCV(pipeB,n_jobs=-1,param_grid=params_bcw,refit=True,verbose=10,cv=2,scoring=scorer)
cvW = ms.GridSearchCV(pipeW,n_jobs=-1,param_grid=params_ww,refit=True,verbose=10,cv=2,scoring=scorer)

generateLearningCurve(cvB, bcwX, bcwY, bcw_trainX, bcw_trainY, bcw_testX, bcw_testY, 'SVM-RBF', 'breast-cancer-wisconsin')
generateLearningCurve(cvW, wwX, wwY, ww_trainX, ww_trainY, ww_testX, ww_testY, 'SVM-RBF', 'thyroid')

#tuning hyper parameters and generate validation curves
train_scores, valid_scores = ms.validation_curve(primalSVM_RBF(), bcwX, bcwY, param_name="gamma_frac", param_range=np.arange(0.2,2.1,0.2), cv=5, scoring="accuracy", n_jobs=1 )
generateValidationCurve(train_scores, valid_scores, 'SVM-RBF', 'breast-cancer-wisconsin', 'gamma_frac', np.arange(0.2,2.1,0.2))

train_scores, valid_scores = ms.validation_curve(primalSVM_RBF(), wwX, wwY, param_name="gamma_frac", param_range=np.arange(0.2,2.1,0.2), cv=5, scoring="accuracy", n_jobs=1 )
generateValidationCurve(train_scores, valid_scores, 'SVM-RBF', 'thyroid', 'gamma_frac', np.arange(0.2,2.1,0.2))


bcw_final_params =cvB.best_params_
ww_final_params =cvW.best_params_

pipeB.set_params(**bcw_final_params)
pipeW.set_params(**ww_final_params)

generateTimingCurve(bcwX,bcwY,pipeB,'SVM-RBF','breast-cancer-wisconsin')
generateTimingCurve(bcwX,bcwY,pipeW,'SVM-RBF','thyroid')

pipeB.set_params(**bcw_final_params)
iterationLC(pipeB,bcw_trainX,bcw_trainY,bcw_testX,bcw_testY,{'SVM__n_iter':np.arange(1,10,1)},'SVM-RBF','breast-cancer-wisconsin')
pipeW.set_params(**ww_final_params)
iterationLC(pipeW,ww_trainX,ww_trainY,ww_testX,ww_testY,{'SVM__n_iter':np.arange(1,75,3)},'SVM-RBF','thyroid')

