# -*- coding: utf-8 -*-

import sklearn.model_selection as ms
import pandas as pd
import numpy as np
from helpers import dtclf_pruned
from generate_curves import generateTimingCurve, generateLearningCurve, generateValidationCurve
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
import sklearn.model_selection as ms
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight
from matplotlib import pyplot as plt
from sklearn.externals.six import StringIO
import pydot

def balanced_accuracy(truth,pred):
    wts = compute_sample_weight('balanced',truth)
    return accuracy_score(truth,pred,sample_weight=wts)

scorer = make_scorer(balanced_accuracy)

def DTpruningVSnodes(clf,alphas,trgX,trgY,dataset):
    '''Dump table of pruning alpha vs. # of internal nodes'''
    out = {}
    for a in alphas:
        clf.set_params(**{'DT__alpha':a})
        clf.fit(trgX,trgY)
        out[a]=clf.steps[-1][-1].numNodes()
        print(dataset,a)
    out = pd.Series(out)
    out.index.name='alpha'
    out.name = 'Number of Internal Nodes'
    out.to_csv('./output/DT_{}_nodecounts.csv'.format(dataset))

    return


#load data
bcw = pd.read_hdf('datasets.hdf','bcw')
bcwX = bcw.drop('class',1).copy().values
bcwY = bcw['class'].copy().values

ww = pd.read_hdf('datasets.hdf','ww')
wwX = ww.drop('Class',1).copy().values
wwY = ww['Class'].copy().values

bcw_trainX, bcw_testX, bcw_trainY, bcw_testY = ms.train_test_split(bcwX, bcwY, test_size=0.2, random_state=4,stratify=bcwY)
ww_trainX, ww_testX, ww_trainY, ww_testY = ms.train_test_split(wwX, wwY, test_size=0.2, random_state=0,stratify=wwY)

# Search for good alphas
alphas = [-1,-1e-3,-(1e-3)*10**-0.5, -1e-2, -(1e-2)*10**-0.5,-1e-1,-(1e-1)*10**-0.5, 0, (1e-1)*10**-0.5]

#before pruning
pipeB = Pipeline([('Scale',StandardScaler()),
                 ('DT',DecisionTreeClassifier(random_state=55))])

pipeW = Pipeline([('Scale',StandardScaler()),
                 ('DT',DecisionTreeClassifier(random_state=55))])

params = {'DT__criterion':['gini', 'entropy'],'DT__class_weight':['balanced']}

cvB = ms.GridSearchCV(pipeB,n_jobs=-1,param_grid=params,refit=True,verbose=10,cv=2,scoring=scorer)
cvW = ms.GridSearchCV(pipeW,n_jobs=-1,param_grid=params,refit=True,verbose=10,cv=2,scoring=scorer)

generateLearningCurve(cvB, bcwX, bcwY, bcw_trainX, bcw_trainY, bcw_testX, bcw_testY, 'DT-unpruned', 'breast-cancer-wisconsin')
generateLearningCurve(cvW, wwX, wwY, ww_trainX, ww_trainY, ww_testX, ww_testY, 'DT-unpruned', 'thyroid')

#with pruning
pipeB = Pipeline([('Scale',StandardScaler()),
                 ('DT',dtclf_pruned(random_state=55))])

pipeW = Pipeline([('Scale',StandardScaler()),
                 ('DT',dtclf_pruned(random_state=55))])

params = {'DT__criterion':['gini', 'entropy'], 'DT__alpha':alphas, 'DT__class_weight':['balanced']}

cvB = ms.GridSearchCV(pipeB,n_jobs=-1,param_grid=params,refit=True,verbose=10,cv=2,scoring=scorer)
cvW = ms.GridSearchCV(pipeW,n_jobs=-1,param_grid=params,refit=True,verbose=10,cv=2,scoring=scorer)

generateLearningCurve(cvB, bcwX, bcwY, bcw_trainX, bcw_trainY, bcw_testX, bcw_testY, 'DT-pruned', 'breast-cancer-wisconsin')
generateLearningCurve(cvW, wwX, wwY, ww_trainX, ww_trainY, ww_testX, ww_testY, 'DT-pruned', 'thyroid')



#tuning hyper parameters and generate validation curves
#train_scores, valid_scores = ms.validation_curve(dtclf_pruned(), bcwX, bcwY, param_name="criterion", param_range=['gini', 'entropy'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'DT', 'breast-cancer-wisconsin', 'criterion', [1,2])
#train_scores, valid_scores = ms.validation_curve(dtclf_pruned(), bcwX, bcwY, param_name="max_depth", param_range=[1,10,20,30], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'DT', 'breast-cancer-wisconsin', 'max_depth', [1,10,20,30])
#train_scores, valid_scores = ms.validation_curve(dtclf_pruned(), bcwX, bcwY, param_name="alpha", param_range=[-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'DT', 'breast-cancer-wisconsin', 'alpha', [-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])

#tuning hyper parameters and generate validation curves
#train_scores, valid_scores = ms.validation_curve(dtclf_pruned(), wwX, wwY, param_name="criterion", param_range=['gini', 'entropy'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'DT', 'thyroid', 'criterion', [1,2])
#train_scores, valid_scores = ms.validation_curve(dtclf_pruned(), wwX, wwY, param_name="max_depth", param_range=[1,10,20,30,40,50], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'DT', 'thyroid', 'max_depth', [1,10,20,30,40,50])
#train_scores, valid_scores = ms.validation_curve(dtclf_pruned(), wwX, wwY, param_name="alpha", param_range=[-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'DT', 'thyroid', 'alpha', [-1,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])


cvB_final_params = cvB.best_params_
cvW_final_params = cvW.best_params_

pipeB.set_params(**cvB_final_params)
pipeW.set_params(**cvW_final_params)

generateTimingCurve(bcwX,bcwY,pipeB,'DT-unpruned','breast-cancer-wisconsin')
generateTimingCurve(bcwX,bcwY,pipeW,'DT-unpruned','thyroid')

DTpruningVSnodes(pipeB,alphas,bcw_trainX,bcw_trainY,'breast-cancer-wisconsin')
DTpruningVSnodes(pipeW,alphas,ww_trainX,ww_trainY,'thyroid')
