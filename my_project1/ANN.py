# -*- coding: utf-8 -*-

import numpy as np
from sklearn.neural_network import MLPClassifier
import sklearn.model_selection as ms
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import make_scorer, accuracy_score
from sklearn.utils import compute_sample_weight
from generate_curves import generateLearningCurve, generateTimingCurve, generateValidationCurve


def balanced_accuracy(truth,pred):
    wts = compute_sample_weight('balanced',truth)
    return accuracy_score(truth,pred,sample_weight=wts)

scorer = make_scorer(balanced_accuracy)

# Load Data
bcw = pd.read_hdf('datasets.hdf','bcw')
bcwX = bcw.drop('class',1).copy().values
bcwY = bcw['class'].copy().values

ww = pd.read_hdf('datasets.hdf','ww')
wwX = ww.drop('Class',1).copy().values
wwY = ww['Class'].copy().values

bcw_trainX, bcw_testX, bcw_trainY, bcw_testY = ms.train_test_split(bcwX, bcwY, test_size=0.2, random_state=4,stratify=bcwY)
ww_trainX, ww_testX, ww_trainY, ww_testY = ms.train_test_split(wwX, wwY, test_size=0.2, random_state=0,stratify=wwY)


pipeB = Pipeline([('Scale',StandardScaler()),
                 ('MLP',MLPClassifier(max_iter=2000,early_stopping=False,random_state=55))])

pipeW = Pipeline([('Scale',StandardScaler()),
                 ('MLP',MLPClassifier(max_iter=2000,early_stopping=False,random_state=55))])


d = bcwX.shape[1]
hiddens_bcw = [10,50,100,150]
alphas = [10**-x for x in np.arange(-1,5.01,1/2)]
alphasM = [10**-x for x in np.arange(-1,9.01,1/2)]
d = wwX.shape[1]
hiddens_ww = [50,100,150,200]

params_bcw = {'MLP__activation':['relu','logistic'],'MLP__alpha':alphas,'MLP__hidden_layer_sizes':hiddens_bcw}
params_ww = {'MLP__activation':['relu','logistic'],'MLP__alpha':alphas,'MLP__hidden_layer_sizes':hiddens_ww}

cvB = ms.GridSearchCV(pipeB,n_jobs=-1,param_grid=params_bcw,refit=True,verbose=10,cv=2,scoring=scorer)
cvW = ms.GridSearchCV(pipeW,n_jobs=-1,param_grid=params_ww,refit=True,verbose=10,cv=2,scoring=scorer)

generateLearningCurve(cvB, bcwX, bcwY, bcw_trainX, bcw_trainY, bcw_testX, bcw_testY, 'ANN', 'breast-cancer-wisconsin')
generateLearningCurve(cvW, wwX, wwY, ww_trainX, ww_trainY, ww_testX, ww_testY, 'ANN', 'thyroid')

#tuning hyper parameters and generate validation curves
#train_scores, valid_scores = ms.validation_curve(MLPClassifier(max_iter=2000), bcwX, bcwY, param_name="activation", param_range=['relu', 'logistic', 'identity', 'tanh'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'ANN', 'breast-cancer-wisconsin', 'activation', [1,2,3,4])
#train_scores, valid_scores = ms.validation_curve(MLPClassifier(max_iter=2000), bcwX, bcwY, param_name="hidden_layer_sizes", param_range=[10,50,100,150], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'ANN', 'breast-cancer-wisconsin', 'hidden_layer_sizes', [10,50,100,150])
#train_scores, valid_scores = ms.validation_curve(MLPClassifier(max_iter=2000), bcwX, bcwY, param_name="learning_rate", param_range=['constant', 'invscaling', 'adaptive'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'ANN', 'breast-cancer-wisconsin', 'learning_rate', [1,2,3])

#tuning hyper parameters and generate validation curves
#train_scores, valid_scores = ms.validation_curve(MLPClassifier(max_iter=2000), wwX, wwY, param_name="activation", param_range=['relu', 'logistic', 'identity', 'tanh'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'ANN', 'thyroid', 'activation', [1,2,3,4])
#train_scores, valid_scores = ms.validation_curve(MLPClassifier(max_iter=2000), wwX, wwY, param_name="hidden_layer_sizes", param_range=[10,50,100,150,200], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'ANN', 'thyroid', 'hidden_layer_sizes', [10,50,100,150,200])
#train_scores, valid_scores = ms.validation_curve(MLPClassifier(max_iter=2000), wwX, wwY, param_name="learning_rate", param_range=['constant', 'invscaling', 'adaptive'], cv=5, scoring="accuracy", n_jobs=1 )
#generateValidationCurve(train_scores, valid_scores, 'ANN', 'thyroid', 'learning_rate', [1,2,3])

bcw_final_params = cvB.best_params_
ww_final_params = cvW.best_params_

pipeB.set_params(**bcw_final_params)
pipeW.set_params(**ww_final_params)

generateTimingCurve(bcwX,bcwY,pipeB,'ANN','breast-cancer-wisconsin')
generateTimingCurve(bcwX,bcwY,pipeW,'ANN','thyroid')
